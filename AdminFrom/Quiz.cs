﻿using System;
using System.Diagnostics;

namespace AdminFrom
{
    class Quiz
    {
        private int id;
        private string question;
        private string answer1;
        private string answer2;
        private string answer3;
        private string answer4;
        private string correct;
        private int userID;
        private bool valid;

        public int Id { get => id; set => id = value; }
        public string Question { get => question; set => question = value; }
        public string Answer1 { get => answer1; set => answer1 = value; }
        public string Answer2 { get => answer2; set => answer2 = value; }
        public string Answer3 { get => answer3; set => answer3 = value; }
        public string Answer4 { get => answer4; set => answer4 = value; }
        public string Correct { get => correct; set => correct = value; }
        public int UserID { get => userID; set => userID = value; }
        public bool Valid { get => valid; set => valid = value; }

        public Quiz(int QuizId, string QuizQuestion, string QuizAnswer1, string QuizAnswer2, string QuizAnswer3, string QuizAnswer4, string QuizCorrect, int QuizUserID,bool QuizValid)
        {
            id = QuizId;
            question = QuizQuestion;
            answer1 = QuizAnswer1;
            answer2 = QuizAnswer2;
            answer3 = QuizAnswer3;
            answer4 = QuizAnswer4;
            correct = QuizCorrect;
            userID = QuizUserID;
            valid = QuizValid;

        }


    }
}
