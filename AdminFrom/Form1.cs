﻿using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using MySqlCommand = MySqlConnector.MySqlCommand;
using MySqlDataAdapter = MySqlConnector.MySqlDataAdapter;

namespace AdminFrom
{
    public partial class Admin : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter adapter;
        int ID = 0;
        string TableName;
        string HISTORY = "History";
        string GEOGRAPHY = "Geography";
        string LITERATURE = "Literature";
        string OTHER = "Other";


        public Admin()
        {
            InitializeComponent();
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            Connection.DataBasaConnection();
            toolTip1.SetToolTip(buttonValid, "Press to accept the question.");
            toolTip2.SetToolTip(buttonDelete, "Press to delete the question. ");
            toolTip3.SetToolTip(buttonHistory, "Historical questions appear here.");
            toolTip4.SetToolTip(buttonGeo, "Geographical questions appear here. ");
            toolTip5.SetToolTip(buttonLi, "Literaturical questions appear here. ");
            toolTip6.SetToolTip(buttonOther, "Questions on other topics appear here. ");
            toolTip7.SetToolTip(dataGridViewQuiz, "A list of questions to approve appears here.");
            toolTip8.SetToolTip(textBoxQuestion, "Here is the Question");
            toolTip9.SetToolTip(textBoxAnswer1, "The first of the possible answers.  ");
            toolTip10.SetToolTip(textBoxAnswer2, "The secound of the possible answers.  ");
            toolTip11.SetToolTip(textBoxAsnwer3, "Third of the possible answers. ");
            toolTip12.SetToolTip(textBoxAnswer4, "Fourth of the possible answers.");
            toolTip13.SetToolTip(textBoxCorrect, "That is the correct answer. ");
            toolTip14.SetToolTip(pictureBoxWelcome, "Hello here!");
            toolTip15.SetToolTip(pictureBox1, "Here you can select the questions.");
        }

        private void reconnection()
        {
            try
            {
               Connection.con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetData(string name)
        {
            Connection.con.Open();
            adapter = new MySqlDataAdapter($"SELECT * FROM {name} where Valid=0", Connection.con);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            dataGridViewQuiz.ReadOnly = true;
            dataGridViewQuiz.DataSource = ds.Tables[0];
            TableName = name;
            Debug.WriteLine(TableName);
            Connection.con.Close();
        }


        private void buttonHistory_Click(object sender, EventArgs e)
        {
            GetData(HISTORY);
        }

        private void buttonGeo_Click(object sender, EventArgs e)
        {
            GetData(GEOGRAPHY);
        }

        private void buttonLi_Click(object sender, EventArgs e)
        {
            GetData(LITERATURE);
        }

        private void buttonOther_Click(object sender, EventArgs e)
        {
            GetData(OTHER);
        }

        private void ClearData()
        {
            textBoxQuestion.Clear();
            textBoxAnswer1.Clear();
            textBoxAnswer2.Clear();
            textBoxAsnwer3.Clear();
            textBoxAnswer4.Clear();
            textBoxCorrect.Clear();
            ID = 0;
        }

        private void dataGridViewQuiz_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {            
            try
            {
                ID = Convert.ToInt32(dataGridViewQuiz.Rows[e.RowIndex].Cells[0].Value.ToString());
                textBoxQuestion.Text = dataGridViewQuiz.Rows[e.RowIndex].Cells[1].Value.ToString();
                textBoxAnswer1.Text = dataGridViewQuiz.Rows[e.RowIndex].Cells[2].Value.ToString();
                textBoxAnswer2.Text = dataGridViewQuiz.Rows[e.RowIndex].Cells[3].Value.ToString();
                textBoxAsnwer3.Text = dataGridViewQuiz.Rows[e.RowIndex].Cells[4].Value.ToString();
                textBoxAnswer4.Text = dataGridViewQuiz.Rows[e.RowIndex].Cells[5].Value.ToString();
                textBoxCorrect.Text = dataGridViewQuiz.Rows[e.RowIndex].Cells[6].Value.ToString();
            }
            catch (Exception i)
            {
                Debug.WriteLine(i.Message); 
            }
        }

        public void ValidateData(string name)
        {
            cmd = new MySqlCommand($"update {name} set Valid=1 where id=@id", Connection.con);
            Connection.con.Open();
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Quiz Validated Successfully!");
            Connection.con.Close();
            ClearData();
        }

        private void buttonValid_Click(object sender, EventArgs e)
        {
            if(ID!=0 && textBoxQuestion.Text!="" && textBoxAnswer1.Text!="" && textBoxAnswer2.Text!="" && textBoxAsnwer3.Text!="" && textBoxAnswer4.Text!="" && textBoxCorrect.Text != "")
            {
                if (TableName == HISTORY)
                {
                    ValidateData(HISTORY);
                    GetData(HISTORY);
                }
                else if (TableName == OTHER)
                {
                    ValidateData(OTHER);
                    GetData(OTHER);
                }
                else if (TableName == LITERATURE)
                {
                    ValidateData(LITERATURE);
                    GetData(LITERATURE);
                }
                else if (TableName == GEOGRAPHY)
                {
                    ValidateData(GEOGRAPHY);
                    GetData(GEOGRAPHY);
                }
            }
            else
            {
                MessageBox.Show("Please Provide Details!");
            }
        }

        public void DeleteData(string name)
        {
            cmd = new MySqlCommand($"Delete From {name} where id=@id", Connection.con);
            Connection.con.Open();
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Quiz Deleted Successfully!");
            Connection.con.Close();
            ClearData();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (ID != 0 && textBoxQuestion.Text != "" && textBoxAnswer1.Text != "" && textBoxAnswer2.Text != "" && textBoxAsnwer3.Text != "" && textBoxAnswer4.Text != "" && textBoxCorrect.Text != "")
            {
                if (TableName == HISTORY)
                {
                    DeleteData(HISTORY);
                    GetData(HISTORY);
                }
                else if (TableName == OTHER)
                {
                    DeleteData(OTHER);
                    GetData(OTHER);
                }
                else if (TableName == LITERATURE)
                {
                    DeleteData(LITERATURE);
                    GetData(LITERATURE);
                }
                else if (TableName == GEOGRAPHY)
                {
                    DeleteData(GEOGRAPHY);
                    GetData(GEOGRAPHY);
                }
            }
            else
            {
                MessageBox.Show("Please Provide Details!");
            }
        }       
    }
}
